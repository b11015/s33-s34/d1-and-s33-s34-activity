/*Load the expressjs module into out application and save it in a variable called express*/
const express = require("express");
/*
	This create an application that uses express and store it as app
	app is our server
*/

// Create an application with expressjs.
// This creates an application that uses express and stores it as app
// app is our server


const app = express();

const port = 4000;

// middleware
// express.json() is a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our req body.
// app.use is used to run a method or another function for our expressjs api

app.use(express.json())

// mock data
let users = [
	{
		username: 'BMadrigal',
		email: 'fateReade@gmail.com',
		password: 'dontTalkAboutMe'
	},
	{
		username: 'Luisa',
		email: 'stronggirl@gmail.com',
		password: 'pressure'
	}
];

let items = [
	{
		name: 'roses',
		price: 170,
		isActive: true
	},
	{
		name: 'tulips',
		price: 250,
		isActive: true
	}
];

// app.get(<endpoint>, <function for req and res>)


// GET METHOD
app.get('/', (req, res) => {

	// Once the route is accessed, we can send a response with the use of res.send()
	// res.send() actually combines writeHead() and end().
		// used to send a response to the client and ends the request

	res.send('Hello from my first expressJS API')

	// res.status(200).send('Hello from my first expressJS API')
});

// SOLUTION in mini activity
app.get('/greeting', (req,res) => {
	res.send('Hello from Batch182-surname')
});

// retrieval of the mock database
app.get('/users', (req, res) => {

	// res.send already stringifies for you
	res.send(users);
	//  or
	// res.json(users);
});

// POST METHOD
app.post('/users', (req, res) => {

	console.log(req.body); //result: {}

	// simulate creating a new user document
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	// push newUser into users array
	users.push(newUser);
	console.log(users);

	// send the updated users array in the client
	res.send(users);
});

// DELETE METHOD
app.delete('/users', (req, res) => {
	users.pop();
	console.log(users);

	// send the updated users array
	res.send(users);
});

// PUT METHOD 
// update user's password
// :index - wildcard
// url: localhost:4000/users/0
app.put('/users/:index', (req, res) => {

	console.log(req.body);

	// an object the contains the value of URL params
	console.log(req.params);

	// paresInt the value of the number coming from req.params
	// ['0'] turns into [0]
	let index = parseInt(req.params.index);

	users[index].password = req.body.password;

	res.send(users[index]);
});

app.put('/users/update/:index', (req, res) => {

	let index = parseInt(req.params.index);
	users[index].username = req.body.username;
	res.send(users[index]);
});


// RETRIEVAL OF SINGLE USER
app.get('/users/getSingleUser/:index', (req, res) => {
	console.log(req.params); //result : {index: '1'}

	// let req.params = [index: '1']
	// parseInt('2')
	let index = parseInt(req.params.index)
	console.log(index); //result : 1

	res.send(users[index]);
	console.log(users[index]);
})

// A C T I V I T Y  S O L U T I O N

app.get('/items', (req, res) => {

	res.send(items);
});

app.post('/items', (req, res) => {
	console.log(req.body);
	let newItem = {
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);
	console.log(items);

	res.send(items)
});

app.put('/items/:index', (req, res) => {

	let index = parseInt(req.params.index);
	items[index].price = req.body.price;

	res.send(items[index])
});

// A C T I V I T Y  S O L U T I O N # 2

app.get('/items/getSingleItem/:index', (req, res) => {
	let index = parseInt(req.params.index)
	console.log(items[index]);

	res.send(items[index]);
	console.log(items[index]);
});

app.put('/items/archive/:index', (req, res) => {

    let index = parseInt(req.params.index);

    items[index].isActive = false

    res.send(items[index]);
    console.log(items);
});

app.put('/items/activate/:index', (req, res) => {

    let index = parseInt(req.params.index);

    items[index].isActive = true

    res.send(items[index]);
    console.log(items);
});



// port
app.listen(port, () => console.log(`Server is running at port ${port}`))